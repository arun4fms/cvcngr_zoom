<?php $currency_symbol = $this->customlib->getSchoolCurrencyFormat(); ?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">

    

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <?php
            if ($this->rbac->hasPrivilege('subject_group', 'can_add')) {
                ?>
               
                <!-- left column -->
            <?php } ?>
            <div <?php
            if ($this->rbac->hasPrivilege('subject_group', 'can_add')) {
                echo "8";
            } else {
                echo "12";
            }
            ?>"> 
                <!-- general form elements -->
                <div class="box box-primary">
                 <?php if ($this->session->flashdata('msg')) {?> <div class="alert alert-success">  <?php echo $this->session->flashdata('msg') ?> </div> <?php }?>
                    <div class="box-header ptbnull">
                        <h3 class="box-title titlefix"><?php echo $this->lang->line('virtualclassreport'); ?> </h3>
                        <div class="box-tools pull-right">
                        </div><!-- /.box-tools -->
                    </div><!-- /.box-header -->
                    <div class="box-body">
                    
                    <div class="row">
                       <div class="col-md-4 col-sm-6">
              <div class="topprograssstart">
                <p class="text-uppercase mt5 clearfix"><i class="fa fa-users ftlayer"></i>Total Users<span class="pull-right"><?php echo $vClassList['report']['total'] ?></span>
               </p>
              <div class="progress-group">
                 <div class="progress progress-minibar">
                      <div class="progress-bar progress-bar-aqua" style="width: 0%"></div>
                    </div>
                  </div>
              </div><!--./topprograssstart-->
           
            </div><!--./col-md-3--> 
                       <div class="col-md-4 col-sm-6">
              <div class="topprograssstart">
                <p class="text-uppercase mt5 clearfix"><i class="fa fa-users ftlayer"></i>Staff Present Today<span class="pull-right"><?php echo $vClassList['report']['totalTeachers'] ?></span>
               </p> 
              <div class="progress-group">
                 <div class="progress progress-minibar">
                      <div class="progress-bar progress-bar-green" style="width: 0%"></div>
                    </div>
                  </div>
              </div><!--./topprograssstart-->
            </div><!--./col-md-3-->
                        

            <div class="col-md-4 col-sm-6">
              <div class="topprograssstart">
                <p class="text-uppercase mt5 clearfix"><i class="fa fa-users ftlayer"></i>Student Present Today<span class="pull-right"><?php echo $vClassList['report']['totalStudents'] ?></span>
               </p>
              <div class="progress-group">
                 <div class="progress progress-minibar">
                      <div class="progress-bar progress-bar-yellow" style="width: 0%"></div>
                    </div>
                  </div>
              </div><!--./topprograssstart-->
            </div><!--./col-md-3--> 
                     </div>
                        
                        <div class="table-responsive mailbox-messages" id="subject_list">
                            <div class="download_label"><?php echo $this->lang->line('subject')." ".$this->lang->line('group')." ".$this->lang->line('list'); ?></div>
 
                        <a class="btn btn-default btn-xs pull-right" id="print" onclick="printDiv()" ><i class="fa fa-print"></i></a> <a class="btn btn-default btn-xs pull-right" id="btnExport" onclick="fnExcelReport();"> <i class="fa fa-file-excel-o"></i> </a>
                            <table class="table table-striped  table-hover " id="headerTable">
                                <thead>
                                    <tr>
                                        <th><?php echo $this->lang->line('class') . "  " . $this->lang->line('name'); ?></th>
                                        <th><?php echo $this->lang->line('students'); ?></th>
                                        <th><?php echo $this->lang->line('teachers'); ?></th>
                                        <th><?php echo $this->lang->line('meetingid'); ?></th>
                                        <th><?php echo $this->lang->line('voicebridge'); ?></th>
                                        <th style="text-align: center;"><?php echo $this->lang->line('action'); ?></th>
                                    </tr>
                                </thead>
                   
                                <tbody>
                                    <?php
                                    if(isset($vClassList['Meetings'])){
                                        foreach ($vClassList['Meetings'] as $subjectgroup) {
                                        
                                        ?>
                                        <tr>
                                            <td class="mailbox-name">
                                                <?php echo $subjectgroup['Name'] ?>
                                            </td>
                                        
                                            <td class="mailbox-name">
                                                <?php echo $subjectgroup['totalStudents'] ?>
                                            </td>
                                          
                                             <td class="mailbox-name">
                                                <?php echo $subjectgroup['moderators'] ?>
                                                 
                                            </td>
                                            
                                             <td class="mailbox-name">
                                                <?php echo $subjectgroup['meetingID'] ?>
                                            </td>
                                            
                                             <td class="mailbox-name">
                                                <?php echo $subjectgroup['voice'] ?>
                                            </td>
                                            
                                            <td class="mailbox-date pull-right no_print">
                                                  <?php
                                                if ($this->rbac->hasPrivilege('subject_group', 'can_delete')) {
                                                    ?>
                                                    <a  href="<?php echo base_url(); ?>admin/Virtualclass/stopClass/<?php echo $subjectgroup['meetingID']; ?>"class="btn btn-default btn-xs no_print"  data-toggle="tooltip" title="End Meeting" onclick="return confirm('<?php echo $this->lang->line('delete_confirm') ?>');">
                                                        <i  class="fa fa-ban"></i>
                                                    </a>
                                                <?php } ?>
                                            </td>
                                        </tr>
                                        <?php
                                    }
                                    }
                                    ?>
                                </tbody>
                                
                                
                                
                                
                                
                                
                                
                                
                            </table><!-- /.table -->



                        </div><!-- /.mail-box-messages -->
                    </div><!-- /.box-body -->
                </div>
            </div><!--/.col (left) -->

            <!-- right column -->

        </div>
        <div class="row">
            <!-- left column -->

            <!-- right column -->
            <div class="col-md-12">

            </div><!--/.col (right) -->
        </div>   <!-- /.row -->
    </section><!-- /.content -->
</div><!-- /.content-wrapper -->


<script>
    var post_section_array = <?php echo json_encode($section_array); ?>;
    $(document).ready(function () {
        var post_class_id = '<?php echo set_value('class_id', 0) ?>';
        // var post_sections= '<?php //print_r($section_array);      ?>';


        if (post_section_array !== null && post_section_array.length > 1) {

            $.each(post_section_array, function (i, elem) {

            });

        }

    


        $(document).on('change', '#class_id', function (e) {
            $('#section_id').html("");
            var class_id = $(this).val();
            var base_url = '<?php echo base_url() ?>';
            var div_data = '<option value=""><?php echo $this->lang->line('select'); ?></option>';
            
             $.ajax({
                type: "GET",
                url: base_url + "sections/getByClass",
                data: {'class_id': class_id},
                dataType: "json",
                success: function (data) {
                    $.each(data, function (i, obj)
                    {
                        div_data += "<option value=" + obj.section_id + ">" + obj.section + "</option>";
                    });

                    $('#section_id').append(div_data);
                }
            });


        });
    });
    
    
   


$(".no_print").css("display","block");
document.getElementById("print").style.display = "block";
  document.getElementById("btnExport").style.display = "block";

        function printDiv() { 
         $(".no_print").css("display","none");
            document.getElementById("print").style.display = "none";
             document.getElementById("btnExport").style.display = "none";
            var divElements = document.getElementById('subject_list').innerHTML;
            var oldPage = document.body.innerHTML;
            document.body.innerHTML = 
              "<html><head><title></title></head><body>" + 
              divElements + "</body>";
            window.print();
            document.body.innerHTML = oldPage;

            location.reload(true);
        }
    
 function fnExcelReport()
{
    var tab_text="<table border='2px'><tr >";
    var textRange; var j=0;
    tab = document.getElementById('headerTable'); // id of table

    for(j = 0 ; j < tab.rows.length ; j++) 
    {     
        tab_text=tab_text+tab.rows[j].innerHTML+"</tr>";
        //tab_text=tab_text+"</tr>";
    }

    tab_text=tab_text+"</table>";
    tab_text= tab_text.replace(/<A[^>]*>|<\/A>/g, "");//remove if u want links in your table
    tab_text= tab_text.replace(/<img[^>]*>/gi,""); // remove if u want images in your table
    tab_text= tab_text.replace(/<input[^>]*>|<\/input>/gi, ""); // reomves input params

    var ua = window.navigator.userAgent;
    var msie = ua.indexOf("MSIE "); 

    if (msie > 0 || !!navigator.userAgent.match(/Trident.*rv\:11\./))      // If Internet Explorer
    {
        txtArea1.document.open("txt/html","replace");
        txtArea1.document.write(tab_text);
        txtArea1.document.close();
        txtArea1.focus(); 
        sa=txtArea1.document.execCommand("SaveAs",true,"Say Thanks to Sumit.xls");
    }  
    else                 //other browser not tested on IE 11
        sa = window.open('data:application/vnd.ms-excel,' + encodeURIComponent(tab_text));  

    return (sa);
}

</script>