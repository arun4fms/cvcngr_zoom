<div class="content-wrapper" style="min-height: 946px;">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>
			<i class="fa fa-mortar-board"></i> Timetable --r</h1>
		</section>
		<!-- Main content -->
		<section class="content">
			<?php if ($this->session->flashdata('msg')) { ?>
			<?php echo $this->session->flashdata('msg') ?>
			<?php } ?>
			<div class="row">
				<div class="col-md-12">
					<div class="box box-primary">
						<div class="box-header with-border">
							<h3 class="box-title"><i class="fa fa-search"></i> Teacher Time Table</h3>
							<div class="box-tools pull-right"></div>
						</div>
						<div class="box-body">
							<?php if (!empty($timetable)) { ?>
							<table class="table table-stripped">
								
								<tbody>
									<tr>
							
										<td class="text text-center">
											<?php if (isset($timetable->meetingId)) { ?>
											<div class="attachment-block clearfix"> <b class="text text-center"><?php echo $this->lang->line('not'); ?> <br><?php echo $this->lang->line('scheduled'); ?></b>
												<br>
											</div>
											<?php } else { foreach ($timetable as $k => $v) { ?>
											
											<div class="info-box" style="padding-top: 15px; padding-bottom: 15px;"> <b class="text-green"><?php echo $this->lang->line('name') ?>: <?php echo $v['meetingName']  ?>
 </br>
 <b class="text-green"><?php echo $this->lang->line('start_time') ?>: <?php echo $v['time']  ?>

										</br></br>
										
											<form method="post" action="<?php  echo base_url(); ?>admin/virtualmeeting/teacherJoin">
												
												<input type="hidden" id="meetingid" name="meetingid" value= <?php echo $v['meetingId'] ; ?>>
												<input type="hidden" id="password" name="password" value= <?php echo $v['attendeePW'] ; ?>>
												
												<button class="btn btn-primary btn-sm" id="submit-buttons" type="submit" >Join Meeting</button>
											</form>
										</div>
										<?php } } ?>
									</td>
								
								</tr>
							</tbody>
						</table>
						<?php } else { ?>
						<div class="alert alert-info">
							<?php echo $this->lang->line('no_record_found'); ?></div>
							<?php } ?>
						</div>
					</div>
				</div>
			</div>
		</section>
	</div>