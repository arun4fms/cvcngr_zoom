<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>backend/dist/css/zoom_addon.css">
<div class="content-wrapper">
    <section class="content-header">
        <h1>
            <i class="fa fa-mortar-board"></i> <?php echo $this->lang->line('live_class'); ?></h1>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title"><i class="fa fa-search"></i> <?php echo $this->lang->line('live_class'); ?></h3>
                        <div class="box-tools pull-right">
                        </div>
                    </div>
                    <div class="box-body">
                        <div class="table-responsive">
                            <div class="download_label"><?php echo $this->lang->line('live_class'); ?></div>
                            
                            <table class="table table-stripped">
										<thead>
											<tr>
												<?php foreach ($timetable as $tm_key=>$tm_value) { ?>
												<th class="text text-center">
													<?php echo $tm_key; ?>
												</th>
												<?php } ?>
											</tr>
										</thead>
										
										
										
										<tbody>
											<tr>
												<?php 
												
											//	die(print_r($timetable));
												
												
												foreach ($timetable as $tm_key=>$tm_value) { ?>
												<td class="text text-center">
													<?php if (!$timetable[$tm_key]) { ?>
													<div class="attachment-block clearfix"> <b class="text text-center"><?php echo $this->lang->line('not'); ?> <br><?php echo $this->lang->line('scheduled'); ?></b>
														<br>
													</div>
													<?php } else { foreach ($timetable[$tm_key] as $tm_k=>$tm_kue) { ?>
													 <div class="info-box" style="padding-top: 15px; "> <b class="text-green"><?php echo $this->lang->line('subject') ?> : <?php echo $tm_kue->title ?> 
													 <br> <strong class="text-green"><?php echo $this->lang->line('time') ?> : <?php echo date("h:i A",strtotime($tm_kue->date)) ?></strong>
													 <br> <strong class="text-green"><?php echo $this->lang->line('duration') ?> : <?php echo $tm_kue->duration ?> <?php echo $this->lang->line('minutes') ?></strong>

												
												
                                                    <?php
    if ($conference_value->status == 0) {
        ?>

                        <a data-placement="left" href="#" class="btn btn-xs label-success p0" data-toggle="modal" data-target="#modal-chkstatus" data-id="<?php echo $tm_kue->id; ?>">
                                                      <span class="" ><i class="fa fa-video-camera"></i> <?php echo $this->lang->line('join') ?></span>
                                                      
                                                        </a>
														
                                                        <?php
                                                     
                                                    }
  


                                                
                                                    ?>
                                            
												
												
												</div>
												<?php } } ?>
											</td>
											<?php } ?>
										</tr>
									</tbody>
									
									
									
										
								</table>
                            
                        </div>
                    </div>
                    
                    <ol>
                        <strong><h4 class="box-title titlefix">Instructions To Use Virtual Class</h4></strong>
								<li>Students should not talk unnecessarily because it causes disturbance to online class&nbsp;</li>
								<li>Students can use the public and private chat facility to interact with teachers</li>
								<li>Try to use a headset and also a room with low &nbsp;noise disturbance area</li>
								<li>For best performance, it&#39;s recommended to use google chrome browser in your device</li>
								<li>For better network signal try to avoid closed rooms to attend online class</li>
							</ol>
                </div>
            </div>
        </div>
    </section>
</div>
<div class="modal fade" id="modal-chkstatus" data-backdrop="static">
    <div class="modal-dialog">
        <form id="form-chkstatus" action="<?php echo site_url('admin/conference/chkstatus'); ?>" method="POST">
            <div class="modal-content">
                <div class="">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <!-- <h4 class="modal-title"> Zoom Details</h4> -->
                </div>
                <div class="modal-body zoom_details">
                  
                </div>
            
            </div>
        </form>
    </div>
</div>

<script type="text/javascript">
    (function ($) {
        "use strict";
        $(document).on('click', 'a.join-btn', function (e) {
            e.preventDefault();
            var id = $(this).data('id');
            var url = $(this).attr('href');
            $.ajax({
                url: "<?php echo site_url("user/conference/add_history") ?>",
                type: "POST",
                data: {"id": id},
                dataType: 'json',
                beforeSend: function () {
                }, success: function (res)
                {
                    if (res.status == 0) {
                    } else if (res.status == 1) {
                        window.open(url, '_blank');
                    }
                },
                error: function (xhr) {
                    alert("Error occured.please try again");
                },
                complete: function () {
                }
            });
        });

             $('#modal-chkstatus').on('shown.bs.modal', function (e) {
            var $modalDiv = $(e.delegateTarget);
          
              var id=$(e.relatedTarget).data('id');


            $.ajax({
                type: "POST",
                url: '<?php echo site_url("user/conference/getlivestatus") ?>',
                data: {'id':id},
                dataType: "JSON",
                beforeSend: function () {
$('.zoom_details').html("");
                    $modalDiv.addClass('modal_loading');
                },
                success: function (data) {
                    

                   $('.zoom_details').html(data.page);
                    $modalDiv.removeClass('modal_loading');
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    $modalDiv.removeClass('modal_loading');
                },
                complete: function (data) {
                    $modalDiv.removeClass('modal_loading');
                }
            });
        })

    })(jQuery);
</script>