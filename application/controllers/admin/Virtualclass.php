<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Virtualclass extends Admin_Controller
{


    function __construct() {
        parent::__construct();
        $this->load->library('form_validation');
        $this->load->model("Virtual_class_model");
        $this->load->model("subjecttimetable_model");
        $this->class_status = true;
        
        
    }

    function index() {
        if (!$this->rbac->hasPrivilege('visitor_book', 'can_view')) {
            access_denied();
        }
    }
    
    /*display teacher time table in admin panel*/
    public function getteachertimetable()
    {
        $json = array();
        $this->form_validation->set_error_delimiters('', '');
        $this->form_validation->set_rules('teacher', $this->lang->line('teacher'), 'trim|required');

        if (!$this->form_validation->run()) {
            $json = array(
                'teacher' => form_error('teacher'),

            );

            $json_array = array('status' => '0', 'error' => $json);
        } else {
            $staff_id = $this->input->post('teacher');
            // $staff_id          = $this->customlib->getStaffID();
            $data['timetable'] = array();
            $days              = $this->customlib->getDaysname();

            foreach ($days as $day_key => $day_value) {
                $data['timetable'][$day_value] = $this->Virtual_class_model->getByStaffandDay($staff_id, $day_value);
            }

            $timetable_page = $this->load->view('admin/virtualclass/_partialgetteachertimetable', $data, true);

            $json_array = array('status' => '1', 'error' => '', 'message' => $timetable_page);

        }

        $this->output
        ->set_content_type('application/json')
        ->set_output(json_encode($json_array));
    }
    
    
    public function vcteachertimetable()
    {

        if (!$this->rbac->hasPrivilege('teachers_time_table', 'can_view')) {
            access_denied();
        }

        $data['title'] = 'My Timetable';
        //$this->session->set_userdata('top_menu', 'Academics');
        //$this->session->set_userdata('sub_menu', 'Academics/timetable/mytimetable');
        $my_role  = $this->customlib->getStaffRole();
        $role     = json_decode($my_role);
        $is_admin = false;

        if ($role->name != "Teacher") {
            $staff_list         = $this->staff_model->getEmployee('teacher');
            $data['staff_list'] = $staff_list;
            $is_admin           = true;
        }

        $staff_id          = $this->customlib->getStaffID();
        $data['timetable'] = array();
        $days              = $this->customlib->getDaysname();
        


        foreach ($days as $day_key => $day_value) {

            $data['timetable'][$day_value] = $this->Virtual_class_model->getByStaffandDay($staff_id, $day_value);
        }

        $this->load->view('layout/header', $data); 
        if ($is_admin) {
            $this->load->view('admin/virtualclass/vcadmintimetable', $data);
        } else {
            $this->load->view('admin/virtualclass/vcmytimetable', $data);
        }
        $this->load->view('layout/footer', $data);
    }
    
    
    public function teacherJoin() {

       if($this->class_status){

       // $apiurl =   $this->input->post('loginurl');
         $meedingid =   $this->input->post('username');
         $classId =   $this->input->post('classid');
         $staffId = $staff_id          = $this->customlib->getStaffID();
        // print_r($classId); die();
        $apiurl = $this->Virtual_class_model->getTeacherUrl($meedingid, $classId, $staffId);
        //print_r($apiurl); die();



         $urlStatus =   $this->Virtual_class_model->getMeetingInfo($meedingid, $classId);
         /*no xml data*/
         //$urlStatus =  true;
         if($urlStatus)
         {
             $this->load->helper('url');
             redirect($apiurl);

         } else {

             $updateStatus = $this->Virtual_class_model->updateRoom($meedingid, $classId);
             if ($updateStatus)
             {
                 $this->load->helper('url'); 
                 redirect($apiurl);

             }  else {
                 $this->session->set_flashdata('msg', '<div class="alert alert-success text-left">' . $this->lang->line('novirtualclass'). '</div>');
                 redirect('admin/virtualclass/vcteachertimetable');

             }
         } 
     } else {
       $this->session->set_flashdata('msg', '<div class="alert alert-success text-left">' . $this->lang->line('vcnotrunning') . '</div>');
       redirect('admin/virtualclass/vcteachertimetable');
   }
}

public function createTimeTable()
{
    if (!$this->rbac->hasPrivilege('class_timetable', 'can_view')) {
        access_denied();
    }

       // $this->session->set_userdata('top_menu', 'Academics');
       // $this->session->set_userdata('sub_menu', 'Academics/timetable');

    $session            = $this->setting_model->getCurrentSession();
    $data['title']      = 'Exam Schedule';
    $data['subject_id'] = "";
    $data['class_id']   = "";
    $data['section_id'] = "";
    $exam               = $this->exam_model->get();
    $class              = $this->class_model->get('', $classteacher = 'yes');
    $data['examlist']   = $exam;
    $data['classlist']  = $class;
    $userdata           = $this->customlib->getUserData();

    $staff                   = $this->staff_model->getStaffbyrole(2);
    $data['staff']           = $staff;
    $data['subject']         = array();
    $feecategory             = $this->feecategory_model->get();
    $data['feecategorylist'] = $feecategory;
    $this->form_validation->set_rules('class_id', $this->lang->line('class'), 'trim|required|xss_clean');
    $this->form_validation->set_rules('section_id', $this->lang->line('section'), 'trim|required|xss_clean');
    $this->form_validation->set_rules('subject_group_id', $this->lang->line('group'), 'trim|required|xss_clean');
    $class_id         = $this->input->post('class_id');
    $section_id       = $this->input->post('section_id');
    $subject_group_id = $this->input->post('subject_group_id');

    $data['class_id']         = $class_id;
    $data['section_id']       = $section_id;
    $data['subject_group_id'] = $subject_group_id;

    if ($this->form_validation->run() == false) {
        $this->load->view('layout/header', $data);
        $this->load->view('admin/virtualclass/vctimetableCreate', $data);
        $this->load->view('layout/footer', $data);
    } else {


        $getDaysnameList          = $this->customlib->getDaysname();
        $data['getDaysnameList']  = $getDaysnameList;
        $subject                  = $this->subjectgroup_model->getGroupsubjects($subject_group_id);

        $data['subject'] = $subject;

        $this->load->view('layout/header', $data);
        $this->load->view('admin/virtualclass/vctimetableCreate', $data);
        $this->load->view('layout/footer', $data);
    }
}

public function savetimetable()
{
        //$json_array = array('status' => '1', 'error' => '', 'message' => $this->lang->line('success_message'));

    $json = array();
    $this->form_validation->set_rules('subject_group_id', $this->lang->line('subject_group'), 'trim|required');
    $this->form_validation->set_rules('day', $this->lang->line('day'), 'trim|required');
    $this->form_validation->set_rules('class_id', $this->lang->line('class'), 'trim|required');
    $this->form_validation->set_rules('section_id', $this->lang->line('section'), 'trim|required');
    foreach ($this->input->post('total_row') as $key => $value) {
        $this->form_validation->set_rules('subject_' . $value, 'Subject', 'trim|required');
        $this->form_validation->set_rules('staff_' . $value, 'Staff', 'trim|required');
        $this->form_validation->set_rules('time_from_' . $value, 'Time From', 'trim|required');
        $this->form_validation->set_rules('time_to_' . $value, 'Time To', 'trim|required');
        $this->form_validation->set_rules('room_no_' . $value, 'Room No', 'trim|required');
    }

    if (!$this->form_validation->run()) {
        $json = array(
            'subject_group_id' => form_error('subject_group_id', '<li>', '</li>'),
            'day'              => form_error('day', '<li>', '</li>'),
            'class_id'         => form_error('class_id', '<li>', '</li>'),
            'section_id'       => form_error('section_id', '<li>', '</li>'),
        );

        foreach ($this->input->post('total_row') as $key => $value) {
            $json['subject_' . $value]   = form_error('subject_' . $value, '<li>', '</li>');
            $json['staff_' . $value]     = form_error('staff_' . $value, '<li>', '</li>');
            $json['time_from_' . $value] = form_error('time_from_' . $value, '<li>', '</li>');
            $json['time_to_' . $value]   = form_error('time_to_' . $value, '<li>', '</li>');
            $json['room_no_' . $value]   = form_error('room_no_' . $value, '<li>', '</li>');
        }

        $json_array = array('status' => '0', 'error' => $json);
    } else {
        $day              = $this->input->post('day');
        $class_id         = $this->input->post('class_id');
        $section_id       = $this->input->post('section_id');
        $subject_group_id = $this->input->post('subject_group_id');
        $total_row        = $this->input->post('total_row');
        $session          = $this->setting_model->getCurrentSession();
        $insert_array     = array();
        $update_array     = array();
        $old_input        = array();
        $prev_array       = $this->input->post('prev_array');
        if (isset($prev_array)) {
            foreach ($prev_array as $prev_arr_key => $prev_arr_value) {
                $old_input[] = $prev_arr_value;
            }
        }
        $preserve_array = array();
        if (isset($total_row)) {
            foreach ($total_row as $total_key => $total_value) {
                $prev_id = $this->input->post('prev_id_' . $total_value);

                if ($prev_id == 0) {
                    $insert_array[] = array(
                        'day'                      => $day,
                        'class_id'                 => $class_id,
                        'section_id'               => $section_id,
                        'subject_group_id'         => $subject_group_id,
                        'subject_group_subject_id' => $this->input->post('subject_' . $total_value),
                        'staff_id'                 => $this->input->post('staff_' . $total_value),
                        'time_from'                => $this->input->post('time_from_' . $total_value),
                        'time_to'                  => $this->input->post('time_to_' . $total_value),
                        'room_no'                  => $this->input->post('room_no_' . $total_value),
                        'session_id'               => $session,
                    );
                } else {
                    $preserve_array[] = $prev_id;
                    $update_array[]   = array(
                        'id'                       => $prev_id,
                        'day'                      => $day,
                        'class_id'                 => $class_id,
                        'section_id'               => $section_id,
                        'subject_group_id'         => $subject_group_id,
                        'subject_group_subject_id' => $this->input->post('subject_' . $total_value),
                        'staff_id'                 => $this->input->post('staff_' . $total_value),
                        'time_from'                => $this->input->post('time_from_' . $total_value),
                        'time_to'                  => $this->input->post('time_to_' . $total_value),
                        'room_no'                  => $this->input->post('room_no_' . $total_value),
                        'session_id'               => $session,
                    );
                }
            }
        }

        $delete_array = array_diff($old_input, $preserve_array);
        $result       = $this->Virtual_class_model->addTimeTable($delete_array, $insert_array, $update_array);

        if ($result) {
            $json_array = array('status' => '1', 'error' => '', 'message' => $this->lang->line('success_message'));
        } else {
            $json_array = array('status' => '2', 'error' => '', 'message' => $this->lang->line('something_wrong'));
        }
    }

    $this->output
    ->set_content_type('application/json')
    ->set_output(json_encode($json_array));
}


public function assignteacher()
{

 if (!$this->rbac->hasPrivilege('class_timetable', 'can_view')) {
    access_denied();
}

       // $this->session->set_userdata('top_menu', 'Academics');
        //$this->session->set_userdata('sub_menu', 'Academics/timetable');




$session            = $this->setting_model->getCurrentSession();
$data['title']      = 'Exam Schedule';
$data['subject_id'] = "";
$data['class_id']   = "";
$data['section_id'] = "";
$exam               = $this->exam_model->get();
$class              = $this->class_model->get('', $classteacher = 'yes');
$data['examlist']   = $exam;
$data['classlist']  = $class;
$userdata           = $this->customlib->getUserData();

$staff                   = $this->staff_model->getStaffbyrole(2);
$data['staff']           = $staff;
$data['subject']         = array();
$feecategory             = $this->feecategory_model->get();
$data['feecategorylist'] = $feecategory;
$this->form_validation->set_rules('class_id', $this->lang->line('class'), 'trim|required|xss_clean');
$this->form_validation->set_rules('section_id', $this->lang->line('section'), 'trim|required|xss_clean');

if ($this->form_validation->run() == true) {
    if (isset($_POST['search'])) {

        $class_id    = $this->input->post('class_id');
        $section_id  = $this->input->post('section_id');
        $days        = $this->customlib->getDaysname();
        $days_record = array();
        foreach ($days as $day_key => $day_value) {
            $class_id              = $this->input->post('class_id');
            $section_id            = $this->input->post('section_id');
            $days_record[$day_key] = $this->Virtual_class_model->getSubjectByClassandSectionDay($class_id, $section_id, $day_key);

        }

        $data['timetable'] = $days_record;

    }

}

$this->load->view('layout/header', $data);
$this->load->view('admin/virtualclass/vctimetable', $data);
$this->load->view('layout/footer', $data);


}

public function getBydategroupclasssection()
{
    $data                = array();
    $data['total_count'] = 1;
    $day                 = $this->input->post('day');
    $class_id            = $this->input->post('class_id');
    $section_id          = $this->input->post('section_id');
    $subject_group_id    = $this->input->post('subject_group_id');
    $subject             = $this->subjectgroup_model->getGroupsubjects($subject_group_id);

    $prev_record = $this->Virtual_class_model->getBySubjectGroupDayClassSection($subject_group_id, $day, $class_id, $section_id);

    $staff         = $this->staff_model->getStaffbyrole(2);
    $data['staff'] = $staff;
    if (empty($prev_record)) {
        $data['prev_record'] = array();
    } else {
        $data['total_count'] = count($prev_record);
        $data['prev_record'] = $prev_record;
    }
    $data['subject']          = $subject;
    $data['day']              = $day;
    $data['class_id']         = $class_id;
    $data['section_id']       = $section_id;
    $data['subject_group_id'] = $subject_group_id;

    $data['html'] = $this->load->view('admin/virtualclass/addrow', $data, true);
    echo json_encode($data);
}








public function delete($id)
{
    if (!$this->rbac->hasPrivilege('subject_group', 'can_delete')) {
        access_denied();
    }

    $data['title'] = 'Fees Master List';

    if ( $this->Virtual_class_model->remove($id)){
        $this->session->set_flashdata('msg', '<div class="alert alert-success text-left">' . $this->lang->line('delete_message') . '</div>');
    }
    else{
        $this->session->set_flashdata('msg', '<div class="alert alert-success text-left">' . $this->lang->line('cannot_delete_this_event') . '</div>');
    }
    redirect('admin/virtualclass/createvirtualclass');
}



public function createvirtualclass()
{




         $this->form_validation->set_rules('name', $this->lang->line('name'), 'required');
         $this->form_validation->set_rules('class_id', $this->lang->line('class'), 'trim|required|xss_clean');
         $this->form_validation->set_rules('sections[]', 'section', 'required');
   
   


   if ($this->form_validation->run() == false) {
    $data['section_array'] = $this->input->post('sections');

} else {


    $className           = preg_replace("/\s+/", "", $this->input->post('name'));
    $classId        = $this->input->post('class_id');
    $session     = $this->setting_model->getCurrentSession();
    $sections = $this->input->post('sections');
    

    if ($this->Virtual_class_model->check_section($classId, $sections)){
            $this->session->set_flashdata('msg', '<div class="alert alert-success text-left">' . $this->lang->line('record_already_exists') . '</div>');
       }

     else if ($this->Virtual_class_model->addRoom($className, $classId, $sections))

    {
        $this->session->set_flashdata('msg', '<div class="alert alert-success text-left">' . $this->lang->line('success_message') . '</div>');
    }
    else{

        $this->session->set_flashdata('msg', '<div class="alert alert-success text-left">' . $this->lang->line('fail') . '</div>');
    }


    redirect('admin/virtualclass/createvirtualclass');
}





       // $this->session->set_userdata('top_menu', 'Academics');
       // $this->session->set_userdata('sub_menu', 'subjectgroup/index');


$vClassList                   = $this->Virtual_class_model->get();
$data['vClassList']       = $vClassList;
$class                 = $this->class_model->get();
$data['classlist']     = $class;

$this->load->view('layout/header', $data);
$this->load->view('admin/virtualclass/createvirtualclass', $data);
$this->load->view('layout/footer', $data);

}



   /* public function delete($id) {
        if (!$this->rbac->hasPrivilege('visitor_book', 'can_delete')) {
            access_denied();
        }
       
        $this->Visitors_model->delete($id);
    }*/
    
    
     /*public function runBBB() {
        if (!$this->rbac->hasPrivilege('visitor_book', 'can_delete')) {
            access_denied();
        }
        
        
                         
                               <!--<button type="submit" value="Add" onclick="window.open(<?php echo $tm_kue->url; ?>)" />
                          
                                

                   <form method="post" action="<?php  echo base_url(); ?>admin/Virtualclass/runBBB">
                      
                          <input type="hidden" id="username" name="username" value=<?php echo $tm_kue->username ; ?> >
                         <input type="hidden" id="usercode" name="usercode" value= <?php echo $tm_kue->usercode; ?>>
                 
                   <button id="submit-buttons" type="submit" >Start Class</button>-->
                   
                   
                   
                   <button onclick="myFunctions(https://www.w3schools.com/','Wizard')">Try it</button>
                   
                   function myFunctions(name,job) {
 var win = window.open("https://www.w3schools.com/", '_blank');
  win.focus(); 
}
         $classcode          = $this->input->post('usercode');
 $unserName = $this->input->post('username');
        
        
         $Server = "http://test-install.blindsidenetworks.com/bigbluebutton/api";
        $Shared_secret = "8cd8ef52e8e101574e400365b55e11a6";
        
        $apiquery = 'fullName=manu&meetingID='.$unserName.'&password='.$classcode.'&redirect=true';
        
        
        $apichecksum = sha1('join'.$apiquery.$Shared_secret);
        $apiurl = $Server.'/join?'.$apiquery.'&checksum='.$apichecksum;
        
        
      
       $this->load->helper('url');


// echo anchor('http://google.com/');

$this->load->helper('url'); 
         redirect($apiurl); 

     }*/


     public function edit($id) {
        if (!$this->rbac->hasPrivilege('visitor_book', 'can_edit')) {
            access_denied();
        }

        $this->form_validation->set_rules('purpose', $this->lang->line('purpose'), 'required');
        
        $this->form_validation->set_rules('name', $this->lang->line('name'), 'required');
        if ($this->form_validation->run() == FALSE) {

            $data['Purpose'] = $this->Visitors_model->getPurpose();
            $data['visitor_list'] = $this->Visitors_model->visitors_list();
            $data['visitor_data'] = $this->Visitors_model->visitors_list($id);
            $this->load->view('layout/header');
            $this->load->view('admin/frontoffice/visitoreditview', $data);
            $this->load->view('layout/footer');
        } else {

            $visitors = array(
                'purpose' => $this->input->post('purpose'),
                'name' => $this->input->post('name'),
                'contact' => $this->input->post('contact'),
                'id_proof' => $this->input->post('id_proof'),
                'no_of_pepple' => $this->input->post('pepples'),
                'date' => date('Y-m-d', $this->customlib->datetostrtotime($this->input->post('date'))),
                'in_time' => $this->input->post('time'),
                'out_time' => $this->input->post('out_time'),
                'note' => $this->input->post('note')
            );
            if (isset($_FILES["file"]) && !empty($_FILES['file']['name'])) {
                $fileInfo = pathinfo($_FILES["file"]["name"]);

                $img_name = 'id' . $id . '.' . $fileInfo['extension'];
                move_uploaded_file($_FILES["file"]["tmp_name"], "./uploads/front_office/visitors/" . $img_name);
                $this->Visitors_model->image_update($id, $img_name);
            }
            $this->Visitors_model->update($id, $visitors);
            redirect('admin/visitors');
        }
    }
    

    public function details($id) {
        if (!$this->rbac->hasPrivilege('visitor_book', 'can_view')) {
            access_denied();
        }

        $data['data'] = $this->Visitors_model->visitors_list($id);
        $this->load->view('admin/frontoffice/Visitormodelview', $data);
    }

    public function download($documents) {
        $this->load->helper('download');
        $filepath = "./uploads/front_office/visitors/" . $documents;
        $data = file_get_contents($filepath);
        $name = $documents;
        force_download($name, $data);
    }

    public function imagedelete($id, $image) {
        if (!$this->rbac->hasPrivilege('visitor_book', 'can_delete')) {
            access_denied();
        }
        $this->Visitors_model->image_delete($id, $image);
    }

    public function check_default($post_string) {
        return $post_string == "" ? FALSE : TRUE;
    }



    
    
    public function virtualclassreport()
    {

       // $this->session->set_userdata('top_menu', 'Academics');
       // $this->session->set_userdata('sub_menu', 'subjectgroup/index');
        
       $vClassList                   = $this->Virtual_class_model->getMeetingReport();
     //  $vClassList                   = $this->Virtual_class_model->get();
        $data['vClassList']       = $vClassList;
       
      /* if($vClassList['Total']==0){
             $this->session->set_flashdata('msg', '<i class="fa fa-check-square-o" aria-hidden="true"></i> ' . $this->lang->line('nomeetings') . '');
             //  shows this in virtual timetable also
        }
        */
        $this->load->view('layout/header', $data);
        $this->load->view('admin/virtualclass/vcreport', $data);
        $this->load->view('layout/footer', $data);
    }

    
     public function stopClass($meetingID)
    {
        $deleteStatus = $this->Virtual_class_model->endmeeting($meetingID);
        if($deleteStatus){
             $this->session->set_flashdata('msg', '<i class="fa fa-check-square-o" aria-hidden="true"></i> ' . $this->lang->line('meetingend') . '');
            redirect('admin/virtualclass/virtualclassreport');
        }
        else
        {
              $this->session->set_flashdata('msg', '<i class="fa fa-check-square-o" aria-hidden="true"></i> ' . $this->lang->line('fail') . '');
            redirect('admin/virtualclass/virtualclassreport');
        }
        $this->load->view('layout/header', $data);
        $this->load->view('admin/virtualclass/vcreport', $data);
        $this->load->view('layout/footer', $data);
    }

   


}
