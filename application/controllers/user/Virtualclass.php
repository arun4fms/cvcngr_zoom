<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Virtualclass extends Student_Controller
{

    public function __construct() 
    {
        parent::__construct();
        $this->load->model("Virtual_class_model");
        $this->load->model("subjecttimetable_model");
        $this->class_status = true;
    }
    public function index()
    {
        $this->load->helper('array');
        $this->session->set_userdata('top_menu', 'Time_table');
        $student_current_class = $this->customlib->getStudentCurrentClsSection();

        $student_id       = $this->customlib->getStudentSessionUserID();
        $student          = $this->student_model->get($student_id);
        $days        = $this->customlib->getDaysname();
        $days_record = array();
        
        
        $login_name = $student['firstname']."-".$student['roll_no'];
        $data['login_url'] =  $this->Virtual_class_model->getStudentUrl($student_current_class->class_id,$student_current_class->section_id, $login_name);
        
        //print_r($data['login_url']); die();
          
        foreach ($days as $day_key => $day_value) {
          $days_record[$day_key] = $this->Virtual_class_model->getparentSubjectByClassandSectionDay($student_current_class->class_id, $student_current_class->section_id, $day_key);
        }
        
          $data['timetable'] = $days_record;
    
          $this->load->view('layout/student/header', $data);
          $this->load->view('user/virtualclass/timetableList', $data);
          $this->load->view('layout/student/footer', $data);
  }
  
    public function studentJoin() {
   
   if($this->class_status){
     
     $apiurl =   $this->input->post('loginurl');
     $meetingid =   $this->input->post('meetingid');
     $studentId =   $this->input->post('studentid');
     $student_current_class = $this->customlib->getStudentCurrentClsSection();
     
    // print_r($apiurl); die();
     
     $urlStatus =   $this->Virtual_class_model->getMeetingInfo($meetingid, $student_current_class->class_id, $studentId);
     /*no xml data*/
     //print_r($urlStatus); die();
    //$urlStatus = true;
     if($urlStatus === true)
     {
         $this->load->helper('url');
         redirect($apiurl);
         
     } else if($urlStatus == 'joined'){
             $this->session->set_flashdata('msg', '<div class="alert alert-success text-left">You have already joined the class, try after sometime </div>');
             redirect('user/Virtualclass');
         }
         else {
         
          $this->session->set_flashdata('msg', '<div class="alert alert-success text-left">" Teacher Not Yet Ready Try After Sometime "</div>');
             redirect('user/Virtualclass');
     } 
 } else {
   $this->session->set_flashdata('msg', '<div class="alert alert-success text-left">' . $this->lang->line('vcnotrunning') . '</div>');
   redirect('user/Virtualclass');
}
}


}
