<style type="text/css">
   *{padding: 0; margin:0;}
   body{ font-family: 'arial';}
   .tc-container{width: 100%;position: relative; text-align: center;padding: 2%;}
   .tc-container tr td{vertical-align: bottom;}
   /*.tc-container{
   width: 100%;
   padding: 2%;
   position: relative;
   z-index: 2;
   }*/
   .tcmybg {
   background:top center;
   position: absolute;
   top: 0;
   left: 0;
   bottom: 0;
   z-index: 1;
   }
   .tc-container tr td h1, h2 ,h3{margin-top: 0; font-weight: normal;}
   /*@media (max-width:210mm) and (min-width:297mm){
   .tc-container{
   margin-top: 200px;
   margin-bottom: 100px;}
   }*/
</style>
<?php
   foreach ($students as $student) {
       ?>
<!-- New Content -->
<section class="content">
   <div class="content-wrapper">
      <div class="content">
         <title>PDF</title>
         <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
         <style>
            hr { 
            border: 0; 
            height: 2px; 
            background-image: linear-gradient(to right, rgba(72,172,46, 1), rgba(72,172,46, 1), rgba(72,172,46, 1)); 
            }
            body {
            margin: 0;
            padding: 0;
            background-color: #ffffff;
            font: 12pt "Tahoma";
            }
            * {
            box-sizing: border-box;
            -moz-box-sizing: border-box;
            }
            .subpage {
            /*padding: 1cm;*/
            border: 1px #eee solid;
            height: 220mm;
            /*outline: 2cm #FFEAEA solid;*/
            }
            @page {
            size: A4;
            margin: 0;
            }
            @media print {
            hr { 
            border: 0; 
            height: 2px; 
            background-image: linear-gradient(to right, rgba(72,172,46, 1), rgba(72,172,46, 1), rgba(72,172,46, 1)); 
            }
            input[type=text] 
            { border: 0px; 
            background: transparent; 
            } 
            input[type=button] {
            visibility: hidden !important;
            }
            textarea { border: none !important; }
            }
         </style>
         <!--<div class="book" >-->
         <div class="page" id="print">
            <!--<div class="subpage">-->
            <div align="center">
               <img width="90%" src="<?php echo base_url(); ?>uploads/print_headerfooter/student_receipt/tchead.jpg">
               <!--<h3></h3>-->
               <!--<h3></h3>-->
               <br>
               <h6> </h6>
            </div>
            <!--<img alt="" height="2" width="100%" src="/images/line.jpg">-->
            <table width="100%">
               <!--<tr><td align="left">sad</td><td align="center">asd</td><td align="right">asd</td></tr>-->
               <tbody>
                  <tr>
                     <td width="33.33%"></td>
                     <td width="33.33%">
                        <center>
                           <strong>
                              <h3 align="center" style=" text-transform: uppercase;font-weight: bold;"> Transfer Certificate</h3>
                           </strong>
                           <center></center>
                        </center>
                     </td>
                     <td width="33.33%"></td>
                  </tr>
               </tbody>
            </table>
            <!--<img alt="" height="2" width="100%" src="/images/line.jpg">-->
            <p> <strong>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp;</strong><strong>TC No.</strong>&nbsp;&nbsp;<strong>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp;</strong><strong>Admission No.&nbsp; &nbsp;&nbsp;</strong><strong><?php echo $student->admission_no ?></strong>
            </p>
            <table align="center" border="1" cellpadding="2" cellspacing="1" style="width: 1000px;">
               <tbody>
                  <tr>
                     <td>
                        <p>&nbsp;1. Name of Pupil</p>
                     </td>
                     <td>&nbsp<?php echo $student->firstname."  ".$student->lastname ?></td>
                  </tr>
                  <tr>
                     <td>
                        <p>&nbsp;2. Father’s/Guardian’s Name</p>
                     </td>
                     <td>&nbsp;<?php echo $student->father_name ?></td>
                  </tr>
                  <tr>
                     <td>
                        <p>&nbsp;3.&nbsp;Nationality</p>
                     </td>
                     <td>&nbsp;India</td>
                  </tr>
                  <tr>
                     <td>
                        <p>&nbsp;4.&nbsp;Religion &amp; Caste</p>
                     </td>
                     <td>&nbsp;<?php echo $student->religion ?></td>
                  </tr>
                  <tr>
                     <td>
                        &nbsp;5. Whether the candidate belongs to
                        <p>&nbsp; &nbsp; &nbsp;Scheduled Caste or Schedule Tribe</p>
                     </td>
                     <td>
                        <textarea style="margin: 0px; width: 601px; height: 44px;"></textarea>
                     </td>
                  </tr>
                  <tr>
                     <td>
                        <p>&nbsp;6. Date of first admission in the School with Class</p>
                     </td>
                     <td>&nbsp;<?php echo $student->admission_date ?></td>
                  </tr>
                  <tr>
                     <td>
                        <p>&nbsp;7. Date of Birth (in Christian Era)&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;a) in figures</p>
                        <p>&nbsp; &nbsp; &nbsp;according to Admission Register &nbsp; &nbsp; &nbsp; b) in words</p>
                     </td>
                     <td>
                        <p><br><br>&nbsp;<?php echo date("d-m-Y", strtotime($student->dob)) ?></p>
                        <p>&nbsp;
                           <textarea cols="20" rows="2" style="margin: 0px; width: 597px; height: 15px;"></textarea>
                        </p>
                     </td>
                  </tr>
                  <tr>
                     <td style="width: 400px;">
                        <p>&nbsp;8.&nbsp;Class in which the pupil last Studied&nbsp; &nbsp; a) in figures</p>
                        <p>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; b) in words</p>
                     </td>
                     <td>
                        <p>&nbsp;
                           <textarea style="margin: 0px; width: 590px; height: 24px;"></textarea>
                        </p>
                        <p>&nbsp;
                           <textarea style="margin: 0px; height: 22px; width: 596px;"></textarea>
                        </p>
                     </td>
                  </tr>
                  <tr>
                     <td>
                        <p>&nbsp;9. School/Board Annual Examination last</p>
                        <p>&nbsp; &nbsp; &nbsp; &nbsp;taken with result&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;</p>
                     </td>
                     <td>&nbsp;
                        <textarea style="margin: 0px; height: 52px; width: 176px;"></textarea>
                     </td>
                  </tr>
                  <tr>
                     <td>
                        <p>&nbsp;10. Whether failed, if so once/twice in the same class</p>
                     </td>
                     <td>
                        <p><br>&nbsp;&nbsp;No</p>
                        <p>&nbsp;</p>
                     </td>
                  </tr>
                  <tr>
                     <td>
                        <p>&nbsp;11. Subject studied / Compulsory / Elective</p>
                     </td>
                     <td>
                        <p>&nbsp;
                           <textarea style="margin: 0px; width: 595px; height: 40px;"></textarea>
                        </p>
                     </td>
                  </tr>
                  <tr>
                     <td>
                        <p>&nbsp;12. Whether qualified for promotion to higher class</p>
                        <p>&nbsp; &nbsp; &nbsp; &nbsp;if so, to which class&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;a) in figures</p>
                        <p>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;b) in words</p>
                     </td>
                     <td>
                        <p>
                           <textarea cols="20" rows="2" style="margin: 0px; width: 603px; height: 34px;"></textarea>
                        </p>
                        <p>
                           <textarea style="margin: 0px; width: 603px; height: 28px;"></textarea>
                        </p>
                     </td>
                  </tr>
                  <tr>
                     <td>
                        <p>&nbsp;13. Month upto which the pupil has paid School dues</p>
                     </td>
                     <td>
                        <textarea cols="20" rows="2" style="margin: 0px; height: 46px; width: 603px;"></textarea>
                     </td>
                  </tr>
                  <tr>
                     <td>
                        <p>&nbsp;14. Any fee concession availed of; If so, the nature</p>
                        <p>&nbsp; &nbsp; &nbsp; of&nbsp;such concessions</p>
                     </td>
                     <td>
                        <textarea cols="20" rows="2" style="margin: 0px; width: 606px; height: 56px;"></textarea>
                     </td>
                  </tr>
                  <tr>
                     <td>
                        <p>&nbsp;15. Total No. of working days in the academic session</p>
                     </td>
                     <td>
                        <textarea cols="20" rows="2" style="margin-left: 0px; margin-right: 0px; width: 605px;"></textarea>
                     </td>
                  </tr>
                  <tr>
                     <td>
                        <p>&nbsp;16. Total No. of working days pupil present in the</p>
                        <p>&nbsp; &nbsp; &nbsp; &nbsp;school</p>
                     </td>
                     <td>
                        <textarea cols="20" rows="2" style="margin: 0px; height: 30px; width: 605px;"></textarea>
                     </td>
                  </tr>
                  <tr>
                     <td>
                        <p>&nbsp;17. Whether NCC Cadet/Boy Scout/Girl Guide (details</p>
                        <p>&nbsp; &nbsp; &nbsp; &nbsp;may be given)&nbsp;</p>
                     </td>
                     <td>&nbsp;
                        <textarea style="margin: 0px; width: 598px; height: 40px;"></textarea>
                     </td>
                  </tr>
                  <tr>
                     <td style="width: 300px;">
                        <p>&nbsp;18. Games played or extra curricular activities in</p>
                        <p>&nbsp; &nbsp; &nbsp; &nbsp;which&nbsp;the pupil usually took part</p>
                        <p>&nbsp; &nbsp; &nbsp; &nbsp;(mention achievement level there in)</p>
                     </td>
                     <td>
                        <textarea cols="20" rows="2" style="margin: 0px; width: 603px; height: 59px;"></textarea>
                     </td>
                  </tr>
                  <tr>
                     <td>
                        <p>&nbsp;19. General conduct</p>
                     </td>
                     <td>
                        <textarea cols="20" rows="2" style="margin-left: 0px; margin-right: 0px; width: 604px;"></textarea>
                     </td>
                  </tr>
                  <tr>
                     <td>
                        <p>&nbsp;20. Date of application for certificate</p>
                     </td>
                     <td>
                        <textarea cols="20" rows="2" style="margin-left: 0px; margin-right: 0px; width: 606px;"></textarea>
                     </td>
                  </tr>
                  <tr>
                     <td>
                        <p>&nbsp;21. Date of issue of certificate</p>
                     </td>
                     <td>&nbsp;<?php echo date("d-m-Y") ?></td>
                  </tr>
                  <tr>
                     <td>
                        <p>&nbsp;22. Reason for leaving the School</p>
                     </td>
                     <td>
                        <textarea cols="20" rows="2" style="margin: 0px; width: 604px; height: 32px;"></textarea>
                     </td>
                  </tr>
                  <tr>
                     <td>
                        <p>&nbsp;23. School to which proceeding</p>
                     </td>
                     <td>&nbsp;
                        <textarea style="margin: 0px; width: 594px; height: 39px;"></textarea>
                     </td>
                  </tr>
                  <tr>
                     <td>
                        <p>&nbsp;24. Any other remarks</p>
                     </td>
                     <td>
                        <textarea cols="20" rows="2" style="margin: 0px; width: 605px; height: 36px;"></textarea>
                     </td>
                  </tr>
               </tbody>
            </table>
            <br><br><br><br>
            <table style="width: 100%;">
               <tbody>
                  <tr>
                     <td style="width: 33.3333%;">
                        <div style="text-align: center;"><strong>Signature of&nbsp;</strong></div>
                        <div style="text-align: center;"><strong>&nbsp;Class-Teacher</strong></div>
                     </td>
                     <td style="width: 33.3333%;">
                        <div style="text-align: center;"><strong>Checked by</strong></div>
                        <div style="text-align: center;"><strong>(Full Name &amp; Designation)</strong></div>
                     </td>
                     <td style="width: 33.3333%;">
                        <div data-empty="true" style="text-align: center;"><strong>Signature of Principal</strong></div>
                        <div data-empty="true" style="text-align: center;"><strong>(Date &amp; Seal)<br></strong></div>
                     </td>
                  </tr>
               </tbody>
            </table>
         </div>
      </div>
   </div>
</section>
<?php
   }
   ?>